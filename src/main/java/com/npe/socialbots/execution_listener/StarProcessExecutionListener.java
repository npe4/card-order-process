package com.npe.socialbots.execution_listener;

import com.fasterxml.jackson.core.type.TypeReference;
import com.npe.dto.kafka.EventType;
import com.npe.dto.kafka.MessageWrapper;
import com.npe.socialbots.utils.ConversionUtils;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.ExecutionListener;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Slf4j
@Component
public class StarProcessExecutionListener implements ExecutionListener {

    @Override
    public void notify(DelegateExecution delegateExecution) throws Exception {
        HashMap<String, Object> variables = ConversionUtils.getMapper().convertValue(delegateExecution.getVariable(EventType.WANT_OFFER_CARD_MESSAGE.name()),
                new TypeReference<HashMap<String, Object>>(){});
        delegateExecution.setVariables(variables);
    }
}
