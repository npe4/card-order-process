package com.npe.socialbots.tasks;

import com.npe.dto.kafka.EventType;
import com.npe.dto.kafka.MessageWrapper;
import com.npe.dto.kafka.in.NotificationDto;
import com.npe.socialbots.utils.ConversionUtils;
import com.npe.socialbots.utils.PayloadUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.admin.NewTopic;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import static com.npe.socialbots.consumer.MessageProcessConsumer.MESSAGE_START;


@Slf4j
@RequiredArgsConstructor
@Component
public class TakeFullNameTask implements JavaDelegate {

    private final KafkaTemplate<String, String> kafkaTemplate;

    private final NewTopic botsTopic;
    
    public void execute(DelegateExecution execution) throws Exception {
        log.info("Send notification");
        MessageWrapper<NotificationDto> messageWrapper =  PayloadUtils.<NotificationDto>prepareMessageWrapper(
                execution.getVariables(),
                new NotificationDto("Привет! Для оформления дебетовой карты пришли мне свои данные. ФИО:"),
                EventType.CARD_ORDER_FIO_REQUEST);
        kafkaTemplate.send(botsTopic.name(), ConversionUtils.javaToJson(messageWrapper));
    }

}
