package com.npe.socialbots.utils;

import com.npe.dto.kafka.EventType;
import com.npe.dto.kafka.MessageWrapper;
import com.npe.dto.kafka.SystemType;
import com.npe.dto.kafka.TargetType;

import java.util.Map;
import java.util.Optional;

public class PayloadUtils {
    public static <T> MessageWrapper<T> prepareMessageWrapper(Map<String, Object> variables, T payload, EventType eventType) {
        return MessageWrapper.<T>builder()
                .correlationId(String.valueOf(variables.get("correlationId")))
                .customerContact(String.valueOf(variables.get("customerContact")))
                .systemType((SystemType) variables.get("systemType"))
                .targetType((TargetType) variables.get("targetType"))
                .eventType(eventType)
                .payload(Optional.of(payload))
                .build();
    }
}
