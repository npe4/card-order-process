package com.npe.socialbots.consumer;

import com.npe.dto.kafka.MessageWrapper;
import org.camunda.bpm.engine.runtime.MessageCorrelationResult;

public interface MessageService {

    MessageCorrelationResult correlateMessage(MessageWrapper<?> message, String messageName);
}
