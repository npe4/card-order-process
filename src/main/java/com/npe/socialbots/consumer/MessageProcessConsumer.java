package com.npe.socialbots.consumer;

import com.npe.dto.kafka.MessageWrapper;
import com.npe.dto.kafka.in.NotificationDto;
import com.npe.socialbots.utils.ConversionUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.RuntimeService;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
public class MessageProcessConsumer {

    private final MessageService messageService;

    public final static String MESSAGE_START = "WANT_OFFER_CARD_MESSAGE";

    @KafkaListener(topics = "${spring.kafka.topics.bot}")
    public void startMessageProcess(String message) {
        MessageWrapper<?> messageWrapper =
                ConversionUtils.jsonToJava(message, MessageWrapper.class);
        if (messageWrapper.getEventType() == null) {
            throw new IllegalArgumentException("EventType can't be null!");
        }
        messageService.correlateMessage(messageWrapper, messageWrapper.getEventType().name());
    }

}
